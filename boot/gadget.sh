#!/bin/sh

cd /sys/kernel/config/usb_gadget/
mkdir -p g1
cd g1

echo 0x1d6b > idVendor  # Linux Foundation
echo 0x0104 > idProduct # Multifunction Composite Gadget
echo 0x0100 > bcdDevice # v1.0.0
echo 0x0200 > bcdUSB    # USB2

# Windows compatibility
echo 0xEF > bDeviceClass    #
echo 0x02 > bDeviceSubClass # Interface Association Descriptor
echo 0x01 > bDeviceProtocol #

# English description
mkdir -p strings/0x409
echo 42   > strings/0x409/serialnumber
echo dalz  > strings/0x409/manufacturer
echo rpi0 > strings/0x409/product

# functions
mkdir -p functions/rndis.usb0        # ethernet (windows compatible)
mkdir -p functions/ecm.usb1          # ethernet (mac compatible)
mkdir -p functions/acm.usb0          # serial
mkdir -p functions/mass_storage.usb0
mkdir -p functions/hid.kbd
mkdir -p functions/hid.mou

# RNDIS
echo RNDIS > functions/rndis.usb0/os_desc/interface.rndis/compatible_id       # windows RNDIS driver
echo 5162001 > functions/rndis.usb0/os_desc/interface.rndis/sub_compatible_id # version 6.0

# mass storage
# echo 0 > functions/mass_storage.usb0/lun.0/nofua # slower, but can unplug without unmounting
echo /mass-storage.img > functions/mass_storage.usb0/lun.0/file

# HID
## keyboard
echo 1 > functions/hid.kbd/protocol
echo 1 > functions/hid.kbd/subclass
echo 8 > functions/hid.kbd/report_length
cat "$BOOTDIR"/report_desc > functions/hid.kbd/report_desc

## mouse
echo 2 > functions/hid.mou/protocol
echo 1 > functions/hid.mou/subclass
echo 6 > functions/hid.mou/report_length
cat "$BOOTDIR"/mouse_report_desc > functions/hid.mou/report_desc

# config (Windows doesn't work with more than one)
mkdir -p configs/c.1/strings/0x409
echo "ECM, RNDIS, HID, storage" > configs/c.1/strings/0x409/configuration # description
echo 500 > configs/c.1/MaxPower

# if they're enabled all at once, HID doesn't work
# it's likely to be an hard limit on the maximum
# number of functions, so ACM has to be sacrificed
ln -s functions/rndis.usb0 configs/c.1/ # first for windows compatibility
ln -s functions/ecm.usb1 configs/c.1/
#ln -s functions/acm.usb0 configs/c.1/
ln -s functions/mass_storage.usb0 configs/c.1/
ln -s functions/hid.kbd configs/c.1/
ln -s functions/hid.mou configs/c.1/

# MS OS Descriptors for Windows compatibility
echo 1       > os_desc/use
echo 0xcd    > os_desc/b_vendor_code
echo MSFT100 > os_desc/qw_sign
ln -s configs/c.1 os_desc

# attach the gadget to the UDC driver
ls /sys/class/udc > UDC
