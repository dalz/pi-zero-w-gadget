#!/bin/sh

export BOOTDIR=$(readlink -f $(dirname $0))
cd "$BOOTDIR"

# set up gadgets
./gadget.sh
./ethernet.sh &
# ./wifi.sh &

TOPDIR=/home/pi
cd $TOPDIR

# start hid daemon
hid/kbdd.py &

# copy events on /dev/hidg0 to /tmp/hidg0 to allow multiple readers
dd if=/dev/hidg0 of=/tmp/hidg0 bs=1 &

# run payloads
. utils/payload-env.sh
for s in payloads/active/*
do
    while . "$s"
    do :; done &
done

get_os
