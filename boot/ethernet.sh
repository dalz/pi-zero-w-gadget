#!/bin/sh

rndis=usb0
ecm=usb1

ip link set up $rndis
ip link set up $ecm

if=""

while [ -z $if ]
do
    [ $(cat /sys/class/net/$rndis/carrier) -eq 1 ] && if=$rndis && sleep 0.5
    [ $(cat /sys/class/net/$ecm/carrier) -eq 1 ] && if=$ecm # give priority to ECM for linux
    sleep 0.5
done

# disable the unused interface
[ $if = $ecm ] && ip link set down $rndis || ip link set down $ecm

ip addr add 10.2.2.1/24 dev $if

# there's only one address available, so cleaning the previous lease is needed
rm /var/lib/misc/dnsmasq.leases
# enable DHCP, disable default gateway and DNS
dnsmasq -i $if -F 10.2.2.2,10.2.2.2,infinite -O 3 -p 0

echo $if > /tmp/interface
