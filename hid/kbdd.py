#!/usr/bin/env python3

import os
import errno
from time import sleep

from keycodes import *

report = bytearray(8)

FIFO = "/tmp/kbd-pipe"

layout = 0

def hold(key, mods):
    report[0] |= mods

    if not key in report:
        for i, k in enumerate(report[2:], 2):
            if k == 0:
                report[i] = key
                hidg.write(report)
                return
        print("Can't press {key}, too many keys held at once")

def release(key, mods):
    report[0] &= ~mods

    for i, k in enumerate(report[2:], 2):
        if k == key:
            report[i] = 0

    hidg.write(report)

def press(key, mods):
    if type(key) is str:
        return press_unicode(key)

    old_mods = report[0]
    hold(key, mods)
    release(key, mods & ~old_mods)

try:
    os.mkfifo(FIFO)
except OSError as oe:
    if oe.errno != errno.EEXIST:
        raise
fifo = open(FIFO)

while True:
    try:
        with open('/dev/hidg0', 'wb', 0) as hidg:
            while True:
                cmd = fifo.readline()

                if not len(cmd):
                    fifo.close()
                    fifo = open(FIFO)
                    continue

                if cmd[-1] == '\n':
                    cmd = cmd[:-1]

                op, rest = cmd.split(' ', 1)
                op = op.upper()

                if op != "STRING":
                    rest = rest.split(' ')

                    if not op in ("LAYOUT", "DELAY"):
                        mods = 0
                        for m in rest[:-1]:
                            mods |= mod_codes[m.upper()]

                        keycode = keycodes[rest[-1]][layout]

                        if keycode is None:
                            # key = rest[-1]
                            continue
                        else:
                            mods |= keycode[0]
                            key = keycode[1]

                if op == "PRESS":
                    press(key, mods)

                elif op == "HOLD":
                    hold(key, mods)

                elif op == "RELEASE":
                    release(key, mods)

                elif op == "STRING":
                    for c in rest:
                        if c == ' ':
                            c = "Spacebar"
                        elif c == '\n':
                            c = "Enter"
                        elif c == '\t':
                            c = "Tab"
                        press(keycodes[c][layout][1], keycodes[c][layout][0])

                elif op == "DELAY":
                    sleep(int(rest[0]) / 1000)

                elif op == "LAYOUT":
                    layout = int(rest[0])

    except Exception as e:
        print(e)
