#!/usr/bin/env python3

# led-watch.py KEY COUNT [FINAL-STATE]
# waits until KEY's LED has changed state COUNT times, then leaves it in the
# FINAL-STATE

from time import time, sleep
from sys import argv

led_codes = {
    "NUM": 1,
    "CAPS": 1 << 1,
    "SCROLL": 1 << 2
}

MAX_DELAY=0.5

key = argv[1].upper()
led = led_codes[key]
num = int(argv[2])

try:
    if argv[3] == "on":
        fstate = True
    else:
        fstate = False
except IndexError:
    fstate = None

# with open("/dev/hidg0", "rb") as hidg:
with open("/tmp/hidg0", "rb") as hidg:
    # go to EOF, don't read previous data
    hidg.seek(0, 2)

    check = True
    on = False
    t = 0

    while True:
        count = 0

        while count < num:
            b = hidg.read(1)

            if not b:
                continue
            b = list(b)[0]

            if not b & led:
                if on:
                    # count the deactivation too
                    on = False
                else:
                    continue
            else:
                on = True

            nt = time()

            if nt - t > MAX_DELAY:
                check = True
                count = 1
            elif check:
                count += 1

            t = nt

        # check if the key was pressed more times
        sleep(MAX_DELAY)
        if hidg.read(1):
            # if it was, wait until it stops being toggled before starting to
            # count again
            check = False
        else:
            break


if fstate is not None and fstate != on:
    with open("/tmp/kbd-pipe", "w") as f:
        if key == "NUM":
            f.write("PRESS NumLock")
        elif key == "CAPS":
            f.write("PRESS CapsLock")
        elif key == "SCROLL":
            f.write("PRESS ScrollLock")
