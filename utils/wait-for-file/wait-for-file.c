#define _POSIX_C_SOURCE 200809L // for strdup

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "unistd.h"
#include "libgen.h"
#include "sys/inotify.h"

#define NAME_MAX 255
#define INOTIFY_BUF_SIZE sizeof(struct inotify_event) + NAME_MAX + 1

int
main(int argc, char* argv[argc + 1])
{
	if (!argc)
		return 1;

	char* path = argv[1];
	char* dir = dirname(strdup(path));
	char* file = basename(strdup(path));

	int fd = inotify_init();
	if (fd < 0)
		return 2;

	// IN_ONLYDIR makes sure that dir exists and is a directory
	int wd = inotify_add_watch(fd, dir, IN_CREATE | IN_MOVED_TO | IN_ONLYDIR);
	if (wd < 0)
		return 3;

	if (!access(path, F_OK))
		// the file already exists
		return 0;

	char buf[INOTIFY_BUF_SIZE];

	while (1) {
		read(fd, buf, INOTIFY_BUF_SIZE);
		if (!strcmp(((struct inotify_event*) buf)->name, file))
			return 0;
	}
}
