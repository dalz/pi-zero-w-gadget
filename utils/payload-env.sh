#!/bin/sh

TARGET_IP=10.2.2.2

log() {
    logfile=/tmp/log
    "$@" 2>>$logfile | tee -a $logfile
}

press() {
    echo press "$@" | sudo tee /tmp/kbd-pipe >/dev/null
}

hold() {
    echo hold "$@" | sudo tee /tmp/kbd-pipe >/dev/null
}

release() {
    echo release "$@" | sudo tee /tmp/kbd-pipe >/dev/null
}

string() {
    echo string "$@" | sudo tee /tmp/kbd-pipe >/dev/null
}

delay() {
    echo delay "$@" | sudo tee /tmp/kbd-pipe >/dev/null
}

waitfor() {
    case "$1" in
        file)
            shift
            "$TOPDIR"/utils/wait-for-file/wait-for-file "$@"
            ;;

        eth)
            waitfor file /var/lib/misc/dnsmasq.leases
            ;;

        led)
            shift
            sudo "$TOPDIR"/hid/led-watch.py "$@"
            ;;

        kbd)
            waitfor file /tmp/hidg0
            ;;
    esac
}

led() {
    case "$1" in
        on)
            echo 1 | sudo tee /sys/class/leds/led0/brightness >/dev/null
            ;;

        off)
            echo 0 | sudo tee /sys/class/leds/led0/brightness >/dev/null
            ;;

        blink)
            for _ in $(seq "${2:-1}"); do
                led on
                led off
            done &
            ;;
    esac
}

get_os() {
    osf=/tmp/target-os
    lock=/tmp/scanning-os

    if [ -f $osf ]; then
        export OS=$(cat $osf)
        return
    fi

    if [ -f $lock ]; then
        waitfor file $osf
        get_os
        return
    fi

    touch $lock

    waitfor eth

    re='\(LINUX\|WINDOWS\|APPLE\)'
    OS=$(sudo nmap -d9 -Pnv -O $TARGET_IP --osscan-guess \
        | sed "s/$re/\U\1/I" | grep -o "$re" | head -1)
    export OS=${OS:-UNKNOWN}

    echo $OS > $osf

    rm $lock
}
