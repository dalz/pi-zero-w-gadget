#!/bin/sh

waitfor led caps 5 off

read -r password < /home/pi/.passwd
get_os

case $OS in
    WINDOWS)
            press super r
            delay 500
            string powershell ".((gwmi win32_volume -f 'label=''zero''').Name+'putty.exe') -ssh pi@10.2.2.1 -pw $password"
            press Enter
            ;;

    LINUX)
            press super a # for GNOME
            press GUI     # for everything else
            delay 500
            string terminal
            press Enter
            delay 500
            string ssh -o StrictHostKeyChecking=no pi@10.2.2.1
            press Enter
            delay 500
            string "$password"
            press Enter
            ;;

    *)
            string os not supported
            ;;
esac
